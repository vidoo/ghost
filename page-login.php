<?php
/*
 * Template name: Login page
 */

if( is_user_logged_in() ) {
	//echo get_permalink( get_option('page_on_front') );
	wp_safe_redirect( get_home_url() );
	exit;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width">
		<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
		<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
		<![endif]-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >
		<?php if(isset($_GET['login']) && $_GET['login'] == 'failed'): ?>
		<div id="login-error" style="background-color: #FFEBE8;border:1px solid #C00;padding:5px;">
			<p>Login failed: You have entered an incorrect Username or password, please try again.</p>
		</div>
		<?php endif; ?>
		<?php
		$args = array(
				'echo'           => true,
				'redirect'       => site_url( get_permalink( get_option('page_on_front') ) ), 
				'form_id'        => 'loginform',
				'label_username' => __( 'Email' ),
				'label_password' => __( 'Password' ),
				'label_remember' => __( 'Remember Me' ),
				'label_log_in'   => __( 'Log In' ),
				'id_username'    => 'user_login',
				'id_password'    => 'user_pass',
				'id_remember'    => 'rememberme',
				'id_submit'      => 'wp-submit',
				'remember'       => false,
				'value_username' => NULL,
				'value_remember' => false
		);

		wp_login_form( $args );
		?>
		<ul>
			<li><a href="<?php echo get_site_url(); ?>"><?php _e('Go home', 'ghost'); ?></a></li>
			<li><a href="<?php echo ''; ?>"><?php _e('Signup', 'ghost'); ?></a></li>
			<li><a href="<?php echo ''; ?>"><?php _e('Forgot your password ?', 'ghost'); ?></a></li>
		</ul>
		<p>&copy; <?php echo date('Y').' '.get_bloginfo('name') ?><br>Fait avec &#10084; à Montréal</p>
	</body>
</html>