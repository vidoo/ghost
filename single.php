<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article>
		<header>
			<?php printf( '<p class="categories">%1$s</p>' , get_the_category_list(', ') ); ?>
			<h1><?php the_title(); ?></h1>
			<p class="authoring"><?php the_date(); ?> <?php _e('by'); ?> <?php the_author(); ?></p>
			<p><?php the_post_thumbnail( 'single-post' ); ?></p>
		</header>
		<section>
			<?php the_content(); ?>
		</section>
		<footer class="meta">
			<?php echo get_the_tag_list('<p class="tag-links"><span>Tags:</span> ','<span>, </span>','</p>'); ?>
		</footer>
		<div class="clear"></div>
	</article>
<?php endwhile; endif; ?>
<?php get_footer(); ?>