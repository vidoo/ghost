<?php
//woocommerce support
add_theme_support( 'woocommerce' );

//security
//remove your WordPress version number from both your head file and RSS feeds :
function ghost_remove_version() {
	return '';
}
add_filter('the_generator', 'ghost_remove_version');

//customizer
function ghost_customize_register( $wp_customize ) {
	//All our sections, settings, and controls will be added here

	$wp_customize->add_section(
		// ID
		'ghost_section_color',
		// Arguments array
		array(
			'title' => __( 'Colors', 'ghost' ),
			'capability' => 'edit_theme_options',
			'description' => __( 'Allows you to edit your theme\'s colors.', 'ghost' ),
			'priority'   => 500,
		)
	);

	/*$wp_customize->add_setting(
		// ID
		'ghost_settings[color_header]',
		// Arguments array
		array(
			'default' => 'right-sidebar',
			'type' => 'option'
		)
	);
	$wp_customize->add_control(
		// ID
		'ghost_control_color',
		// Arguments array
		array(
			'type' => 'radio',
			'label' => __( 'Header color', 'ghost' ),
			'section' => 'ghost_section_color',
			'choices' => array(
				'left-sidebar' => __( 'Left sidebar', 'ghost' ),
				'right-sidebar' => __( 'Right sidebar', 'ghost' )
			),
			// This last one must match setting ID from above
			'settings' => 'ghost_settings[color_header]'
		)
	);*/

	// add color picker setting
	$wp_customize->add_setting( 'ghost_settings_color_header', array(
		'default' => '#2F7DD2'
	) );

	// add color picker control
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ghost_settings_color_header', array(
		'label' => __( 'Header color', 'ghost' ),
		'section' => 'ghost_section_color',
		'settings' => 'ghost_settings_color_header',
	) ) );

	$wp_customize->add_setting( 'ghost_settings_color_link', array(
		'default' => '#1c94c6'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ghost_settings_color_link', array(
		'label' => __( 'Links color', 'ghost' ),
		'section' => 'ghost_section_color',
		'settings' => 'ghost_settings_color_link',
	) ) );
}
add_action( 'customize_register', 'ghost_customize_register' );

function ghost_customize_css()
{
    ?>
    <style type="text/css">
        #page>header { background-color:<?php echo get_theme_mod('ghost_settings_color_header'); ?>; }
        a { color:<?php echo get_theme_mod('ghost_settings_color_link'); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'ghost_customize_css');


//BEST PRACTICE
define('DISALLOW_FILE_EDIT', true);

function ghost_scripts_and_styles() {

	global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

	if (!is_admin()) {

		//wp_register_script( 'ghost-modernizr', get_stylesheet_directory_uri() . '/assets/js/libs/modernizr.custom.min.js', array(), '2.5.3', false );
		
		//google font
		wp_register_style( 'google-font-montserrat', '//fonts.googleapis.com/css?family=Montserrat' );
		wp_register_style( 'google-font-opensans', '//fonts.googleapis.com/css?family=Open+Sans:400,700' );
		wp_register_style( 'google-font-breeserif', '//fonts.googleapis.com/css?family=Bree+Serif' );
		
		//theme style
		wp_register_style( 'ghost-stylesheet', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), '', 'all' );
		wp_register_style( 'ghost-ie-only', get_stylesheet_directory_uri() . '/assets/css/ie.css', array(), '' );

		//adding scripts file in the footer
		wp_register_script( 'ghost-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );

		// enqueue styles and scripts
		//wp_enqueue_script( 'ghost-modernizr' );

		wp_enqueue_style( 'google-font-montserrat' );
		wp_enqueue_style( 'google-font-opensans' );
		wp_enqueue_style( 'google-font-breeserif' );
		wp_enqueue_style( 'ghost-stylesheet' );
		wp_enqueue_style( 'ghost-ie-only' );

		$wp_styles->add_data( 'ghost-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'ghost-js' );
	}
}

add_action('wp_enqueue_scripts', 'ghost_scripts_and_styles');

add_image_size( 'single-post', 1024, 300, true );

register_nav_menus(array(
	'primary' => __('Main menu', 'ghost')
));

/*
add_filter('wp_nav_menu_items', 'add_login_logout_link', 10, 2);

function add_login_logout_link($items, $args) {
	//$loginoutlink = wp_loginout('index.php', false);
	$loginlink = wp_login_url('index.php', false);
	$items .= '<li id="login-link"><a href="' . $loginlink . '">Log in</a></li>';
	return $items;
}
*/

add_action( 'wp_login_failed', 'ghost_login_fail' );  // hook failed login

function ghost_login_fail( $username ) {
	$referrer = wp_get_referer();  // where did the post submission come from?
	// if there's a valid referrer, and it's not the default log-in screen
	if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
		if ( !strstr($referrer, 'login=failed') ) {
			wp_safe_redirect( add_query_arg( 'login', 'failed' , $referrer ) );
		}else{
			wp_safe_redirect( $referrer );
		}
		exit;
	}
}

//if user or password is blank prevent redirection to wp-login.php
add_filter( 'authenticate', 'ghost_authenticate_username_password', 30, 3);

function ghost_authenticate_username_password( $user, $username, $password )
{
    if ( is_a($user, 'WP_User') ) { return $user; }

    if ( empty($username) || empty($password) )
    {
        $error = new WP_Error();
        $user  = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.'));

        return $error;
    }
}