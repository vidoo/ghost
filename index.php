<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article>
		<div class="content">
			<header>
				<?php printf( '<p class="categories">%1$s</p>' , get_the_category_list(', ') ); ?>
				<h1><?php the_title(); ?></h1>
				<p class="authoring"><?php the_date(); ?> <?php _e('by'); ?> <?php the_author(); ?></p>
			</header>
			<section>
				<?php the_excerpt(); ?>
				<p><a href="<?php the_permalink(); ?>"><?php _e('Read more'); ?></a></p>
			</section>
		</div>
		<footer class="meta">
			<p class="authoring">
				<?php echo get_the_date(); ?><br>
				<?php _e('by'); ?> <?php the_author(); ?>
			</p>
			<?php echo get_the_tag_list('<p class="tag-links"><span>Tags:</span> ','<span>, </span>','</p>'); ?>
			<p><?php the_post_thumbnail( 'thumbnail' ); ?></p>
		</footer>
		<div class="clear"></div>
	</article>
<?php endwhile; endif; ?>
<div class="pagination">
	<div class="previous-page"><?php previous_posts_link('<'); ?></div>
	<div class="next-page"><?php next_posts_link('>'); ?></div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>